#! /usr/bin/python3


import unittest

import util


# pylint: disable=I0011, E1101
class TestUtil(unittest.TestCase):
    def test_generate_paths_dfs(self):
        l11 = util.Tree('l11', 'true')
        l12 = util.Tree('l12', 'false')

        m1 = util.Tree('m1', 'false')
        m1.add(l11)
        m1.add(l12)

        l21 = util.Tree('l21', 'false')

        m2 = util.Tree('m2', 'true')
        m2.add(l21)

        r = util.Tree('r', 'false')
        r.add(m1)
        r.add(m2)

        paths = util.generate_paths_dfs(r)
        
        self.assertEqual(3, len(paths))
        self.assertIn((['r', 'm1', 'l11'], True), paths)
        self.assertIn((['r', 'm1', 'l12'], False), paths)
        self.assertIn((['r', 'm2', 'l21'], True), paths)
