#! /usr/bin/python3


import unittest
import os

import util


# pylint: disable=I0011, E1101
class TestTreeParser(unittest.TestCase):
    def test_parse_tree_str(self):
        example_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'example.csv')
        trees = util.read_trees(example_path)
        self.assertEqual(1, len(trees))
        self.assertIn('1', trees)

        tree = trees['1']

        self.assertEqual('t1', tree.tag)
        self.assertFalse(tree.is_content)
        self.assertEqual(2, len(tree.children))

        child1, child2 = tree.children
        
        self.assertEqual('t2', child1.tag)
        self.assertTrue(child1.is_content)
        self.assertEqual(0, len(child1.children))

        self.assertEqual('t3', child2.tag)
        self.assertFalse(child2.is_content)
        self.assertEqual(1, len(child2.children))

        child21, = child2.children

        self.assertEqual('t4', child21.tag)
        self.assertTrue(child21.is_content)
        self.assertEqual(0, len(child21.children))
