#! /usr/bin/python3


import sys
import os
import unittest

sys.path.insert(0, os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))), 'src'))

import test_tree_parser
import test_util


if __name__ == '__main__':
    for module in [test_tree_parser, test_util]:
        test_suite = unittest.TestLoader().loadTestsFromModule(module)
        unittest.TextTestRunner().run(test_suite)
