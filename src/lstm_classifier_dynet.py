#! /usr/bin/python3


import random

import dynet_config
dynet_config.set(autobatch=1)
import dynet

import util


# pylint: disable=E1101
class LSTMClassifier(object):
    """This classifier assigns labels to each path from root to leaf of an HTML tree."""
    def __init__(self, input_map, num_layers, hidden_size, dropout, from_file=None):
        """Construct the network."""
        self.input_map = input_map
        self.dropout = dropout
        self.model = dynet.ParameterCollection()

        vocab_size = input_size = len(input_map)
        output_size = 1
        self.lstm = dynet.LSTMBuilder(num_layers, input_size, hidden_size, self.model)
        self.lookup = self.model.add_lookup_parameters((vocab_size, input_size))
        self.W = self.model.add_parameters((output_size, hidden_size))
        self.b = self.model.add_parameters((output_size))
        self.trainer = dynet.AdamTrainer(self.model)

        if from_file is not None:
            self.load_model(from_file)

    def _train_single_input(self, net_input, label):
        """Train a single input sequence. Return the loss."""
        state = self.lstm.initial_state()
        W = dynet.parameter(self.W)
        b = dynet.parameter(self.b)

        for item in net_input:
            state = state.add_input(self.lookup[self.input_map[item]])
        out_dropout = dynet.dropout(W * state.output() + b, self.dropout)
        prob = dynet.logistic(out_dropout)
        return -label * dynet.log(prob) - (1 - label) * dynet.log(1 - prob)

    def train(self, net_inputs, batch_size=1, epochs=1):
        """Train a number of input sequences."""
        net_inputs_copy = net_inputs.copy()
        for e in range(1, epochs + 1):
            random.shuffle(net_inputs_copy)
            b = 0
            e_loss = 0
            for batch in util.chunks(net_inputs_copy, batch_size):
                b += 1
                dynet.renew_cg()
                losses = []
                for seq, label in batch:
                    losses.append(self._train_single_input(seq, label))
                loss = dynet.esum(losses)
                print('batch {}, loss: {}'.format(b, loss.value()), end='\r')
                e_loss += loss.value()
                loss.backward()
                self.trainer.update()
            print('epoch {}, loss: {}'.format(e, e_loss))
    
    def predict(self, seq):
        """Predict a label for an input sequence."""
        dynet.renew_cg()
        W = dynet.parameter(self.W)
        b = dynet.parameter(self.b)
        state = self.lstm.initial_state()

        for item in seq:
            state = state.add_input(self.lookup[self.input_map[item]])
        return round(dynet.logistic(W * state.output() + b).value())
        
    def test(self, net_inputs):
        """
        Predict a label for all sequences in "net_inputs".
        Return a list of predicted labels (1 or 0) and a list of true labels.
        """
        true_labels = []
        predicted_labels = []
        c = 0
        for seq, label in net_inputs:
            c += 1
            print('[{}/{}]'.format(c, len(net_inputs)), end='\r')
            true_labels.append(label)
            predicted_labels.append(self.predict(seq))
        print()
        return predicted_labels, true_labels
    
    def save_model(self, save_path):
        """Store the model in a file."""
        self.model.save(save_path)
    
    def load_model(self, load_path):
        """Load the model from a file."""
        self.model.populate(load_path)


def get_net_inputs(trees, terminator=None):
    """
    Transform "trees" into inputs accepted by "LSTMClassifier".
    "terminator" can be set to a string, which will then be appended to all paths.
    """
    net_inputs = []
    all_tags = set()
    if terminator is not None:
        all_tags.add(terminator)
    for paths in map(util.generate_paths_dfs, trees):
        for path, label in paths:
            all_tags.update(path)
            if terminator is not None:
                path.append(terminator)
            # True -> 1, False -> 0
            net_inputs.append((path, int(label)))
    return net_inputs, util.get_int_map(all_tags)
