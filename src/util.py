#! /usr/bin/python3

import sys
import csv
import random
from collections import OrderedDict
import nltk

import tree_parser


#increase CSV field size to system max
maxInt = sys.maxsize
decrement = True

while decrement:
    # decrease the maxInt value by factor 10 
    # as long as the OverflowError occurs.

    decrement = False
    try:
        csv.field_size_limit(maxInt)
    except OverflowError:
        maxInt = int(maxInt/10)
        decrement = True

class Tree(object):
    """Simple tree class."""
    def __init__(self, tag, is_content_str, content_str, children=None):
        """Initialize the node value and add children if any."""
        self.tag = tag
        self.is_content = is_content_str == 'true'
        self.content = content_str
        self.children = []
        if children is not None:
            for tag, bool_str, content, sub_children in chunks(children, 4):
                self.add(Tree(tag, bool_str, content, sub_children))

    def add(self, child):
        """Add "child" to the list of children. Must be a "Tree" object."""
        assert isinstance(child, self.__class__)
        self.children.append(child)
    
    @property
    def is_leaf(self):
        """Return True if the node has no children, False otherwise."""
        return len(self.children) == 0

    def __repr__(self):
        result = '(' + self.tag + ' ' + str(self.is_content)
        for child in self.children:
            result += ' ' + str(child)
        return result + ')'


def chunks(l, n):
    """Yield successive n-sized chunks from "l"."""
    for i in range(0, len(l), n):
        yield l[i:i + n]


def read_trees(file_path):
    """Read all trees from a file and return Tree objects."""
    result = OrderedDict()
    with open(file_path, encoding='utf-8') as hfile:
        for row in csv.DictReader(hfile):
            print(row['id'])
            result[row['id']] = tree_parser.parse_tree_str(row['tree'])
    return result


def generate_paths_dfs(node):
    """
    Perform a DFS on the node and save the paths.
    The result is a list of tuples "(path, label)" where a path is a list of HTML tags and the label
    is either True (content) if the path contained at least one HTML tag marked as content or False
    (boilerplate) otherwise.
    """
    if node.is_leaf:
        return [([node.tag], node.is_content)]

    paths = []
    for child in node.children:
        for path, is_content in generate_paths_dfs(child):
            paths.append(([node.tag] + path, is_content or node.is_content))
    return paths


def get_int_map(items, offset=0):
    """Return a dict that maps each unique item in "items" to a unique int ID."""
    # we sort the items to guarantee that we get the same mapping every time for a fixed input
    return {item: i + offset for i, item in enumerate(sorted(set(items)))}


def balance_data(net_inputs):
    """
    Return a balanced set, i.e. remove some paths until the data contains the same number of
    positives and negatives.
    """
    pos = []
    neg = []
    for path, label in net_inputs:
        if label:
            pos.append((path, label))
        else:
            neg.append((path, label))
    min_len = min(len(pos), len(neg))
    pos_sample = random.sample(pos, min_len)
    neg_sample = random.sample(neg, min_len)
    return pos_sample + neg_sample

def partofspeech(text):
    """
    Returns a vector of the appearance of certain tags normalized by the size of the input text.
    Need to sort the dict because of the need of the same mapping for the same input values.
    """
    tagged = nltk.pos_tag(nltk.word_tokenize(text))
    
    #Initialize possible tags with 0 in dictionary
    tags = {
        "$": 0,
        "''": 0,
        "(": 0,
        ")": 0,
        ",": 0,
        "--": 0,
        ".": 0,
        ":": 0,
        "CC": 0,
        "CD": 0,
        "DT": 0,
        "EX": 0,
        "FW": 0,
        "IN": 0,
        "JJ": 0,
        "JJR": 0,
        "JJS": 0,
        "LS": 0,
        "MD": 0,
        "NN": 0,
        "NNP": 0,
        "NNPS": 0,
        "NNS": 0,
        "PDT": 0,
        "POS": 0,
        "PRP": 0,
        "PRP$": 0,
        "RB": 0,
        "RBR": 0,
        "RBS": 0,
        "RP": 0,
        "SYM": 0,
        "TO": 0,
        "UH": 0,
        "VB": 0,
        "VBD": 0,
        "VBG": 0,
        "VBN": 0,
        "VBP": 0,
        "VBZ": 0,
        "WDT": 0,
        "WP": 0,
        "WP$": 0,
        "WRB": 0,
        "``": 0
    }

    #Adding values of tags to the dict and normnalize it by the length of the string
    for (word, tag) in tagged:
        tags[tag] = tags[tag] + (1/len(tagged))

    # Order list by keys
    tagso = OrderedDict(sorted(tags.items()))

    #Return vector
    return tagso.values()

def reformattext(text):
    """
    Reformat the manipulated string (#openbracket)
    """

    result = text.replace("#openbracket","(").replace("#closedbracket", ")")
    result = result.encode("unicode_escape").decode('utf-8')

    return result