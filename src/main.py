#! /usr/bin/python3

import argparse
import os

from sklearn.metrics import classification_report, confusion_matrix

import util
from lstm_classifier_keras import get_net_inputs, LSTMClassifier


def main():
    ap = argparse.ArgumentParser()
    ap.add_argument('INPUT', help='A csv file that contains trees')
    ap.add_argument('-l', '--num_layers', type=int, default=1, help='The number of RNN layers.')
    ap.add_argument('-u', '--hidden_units', type=int, default=64, help='The number of hidden units.')
    ap.add_argument('-d', '--dropout', type=float, default=0, help='The dropout percentage.')
    ap.add_argument('--train', type=int, nargs=2, help='Use index X to index Y for training')
    ap.add_argument('-e', '--epochs', type=int, default=1, help='The number of epochs.')
    ap.add_argument('-b', '--batch_size', type=int, default=1, help='The batch size.')
    ap.add_argument('--test', type=int, nargs=2, help='Use index X to index Y for testing')
    ap.add_argument('--model', help='Load and save the model from/in a file.')
    ap.add_argument('--balanced', action='store_true', help='Balance the training set')
    args = ap.parse_args()


   

    # read data
    print('reading data...')
    trees = list(util.read_trees(args.INPUT).values())
    _, input_map = get_net_inputs(trees)
    print('vocabulary: {} tags'.format(len(input_map)))

    # model
    if args.model and os.path.exists(args.model):
        print('loading model from {}...'.format(args.model))
        clf = LSTMClassifier(input_map, args.num_layers, args.hidden_units, args.dropout, args.model)
    else:
        print('compiling model...')
        clf = LSTMClassifier(input_map, args.num_layers, args.hidden_units, args.dropout)
    
    # training
    if args.train:
        i1, i2 = args.train
        trees_train = trees[i1:i2 + 1]
        print('training with {} trees...'.format(len(trees_train)))
        train_inputs, _ = get_net_inputs(trees_train)
        if args.balanced:
            train_inputs = util.balance_data(train_inputs)
        num_batches = int(len(train_inputs) / args.batch_size)
        print('{} inputs per epoch, {} batches'.format(len(train_inputs), num_batches))
        clf.train(train_inputs, args.batch_size, args.epochs)

        if args.model:
            print('saving model to {}...'.format(args.model))
            clf.save_model(args.model)
    
    # testing
    if args.test:
        i1, i2 = args.test
        trees_test = trees[i1:i2 + 1]
        print('testing with {} trees...'.format(len(trees_test)))
        test_inputs, _ = get_net_inputs(trees_test)
        predicted_labels, true_labels = clf.test(test_inputs)
        print('results:')
        print(classification_report(true_labels, predicted_labels))
        print(confusion_matrix(true_labels, predicted_labels))


if __name__ == '__main__':
    main()
