#! /usr/bin/python3


import random

import keras
import numpy

import util


# pylint: disable=E1101
class LSTMClassifier(object):
    """This classifier assigns labels to each path from root to leaf of an HTML tree."""
    def __init__(self, input_map, num_layers, hidden_size, dropout, from_file=None):
        """Construct the network."""
        self.input_map = input_map
        # add 1 for padding
        self.vocab_size = len(input_map) + 1

        if from_file is None:
            self.model = keras.Sequential()
            self.model.add(keras.layers.InputLayer(input_shape=(None, self.vocab_size)))
            for _ in range(num_layers - 1):
                self.model.add(keras.layers.LSTM(hidden_size, return_sequences=True))
            self.model.add(keras.layers.LSTM(hidden_size, return_sequences=False))
            if dropout > 0:
                self.model.add(keras.layers.Dropout(dropout))
            self.model.add(keras.layers.Dense(1, activation='sigmoid'))
            self.model.compile(loss='binary_crossentropy', optimizer='adam')
        else:
            self.load_model(from_file)

    def _batch_generator(self, net_inputs, batch_size):
        """Generate batches for training/testing."""
        while True:
            for chunk in util.chunks(net_inputs, batch_size):
                # this happens if the last chunk is too small
                if len(chunk) < batch_size:
                    break
                batch_x = []
                batch_y = []
                for path, label in chunk:
                    path_mapped = list(map(self.input_map.get, path))
                    #print(path_mapped)
                    # convert to one-hot vectors
                    batch_x.append(keras.utils.to_categorical(path_mapped, self.vocab_size))
                    batch_y.append(label)
                yield keras.preprocessing.sequence.pad_sequences(batch_x), batch_y
            #print(batch_x)
    
    def train(self, net_inputs, batch_size=1, epochs=1):
        """Train a number of input sequences."""
        steps_per_epoch = int(len(net_inputs) / batch_size)
        self.model.fit_generator(self._batch_generator(net_inputs, batch_size), steps_per_epoch, epochs)
        
    def test(self, net_inputs):
        """
        Predict a label for all sequences in "net_inputs".
        Return a list of predicted labels (1 or 0) and a list of true labels.
        """
        batch_x, batch_y = next(self._batch_generator(net_inputs, len(net_inputs)))
        pred = self.model.predict_on_batch(batch_x).flatten()
        return numpy.around(pred), batch_y
        
    def save_model(self, save_path):
        """Store the model in a file."""
        self.model.save(save_path)
    
    def load_model(self, load_path):
        """Load the model from a file."""
        self.model = keras.models.load_model(load_path)


def get_net_inputs(trees, terminator=None):
    """
    Transform "trees" into inputs accepted by "LSTMClassifier".
    "terminator" can be set to a string, which will then be appended to all paths.
    """
    net_inputs = []
    all_tags = set()
    if terminator is not None:
        all_tags.add(terminator)
    for paths in map(util.generate_paths_dfs, trees):
        for path, label in paths:
            all_tags.update(path)
            if terminator is not None:
                path.append(terminator)
            # True -> 1, False -> 0
            # print(path, int(label))
            net_inputs.append((path, int(label)))
            
    # add an offset of 1 to reserve 0 for padding
    return net_inputs, util.get_int_map(all_tags, offset=1)
