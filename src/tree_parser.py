#! /usr/bin/python3


import util
import pyparsing


# create a recursive grammar for parsing the tree
P_OP = pyparsing.Suppress(pyparsing.Literal('('))
P_CL = pyparsing.Suppress(pyparsing.Literal(')'))
BOOL = pyparsing.Literal('true') | pyparsing.Literal('false')
TAG = pyparsing.Word(pyparsing.printables)
TEXT = pyparsing.QuotedString('******')
NODE = pyparsing.Forward()
NODE << P_OP + TAG + BOOL + TEXT + pyparsing.Group(pyparsing.ZeroOrMore(NODE)) + P_CL
#NODE << P_OP + TAG + BOOL + pyparsing.Group(pyparsing.ZeroOrMore(NODE)) + P_CL

def parse_tree_str(tree_str):
    """Parse the tree in "tree_str" and create a Tree object."""
    # we only allow one root node
    
    parsed = NODE.parseString(tree_str)


    assert len(parsed) == 4

    root_tag, root_bool_str, root_str, root_children = parsed

    return util.Tree(root_tag, root_bool_str, root_str, root_children)
